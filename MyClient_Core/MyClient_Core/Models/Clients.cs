﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyClient_Core.Models
{
    public class Clients
    {
        [Key]
        [Display(Name = "Cliente ID")]
        public int Clieid { get; set; }

        [Required(ErrorMessage = "Introduce nombre del cliente")]
        [Display(Name = "Nombre del Cliente")]
        public String Cliename { get; set; }

        [Required(ErrorMessage = "Introduce la edad del cliente")]
        [Display(Name = "Edad del Cliente")]
        [Range(18, 100)]
        public int Age { get; set; }

        [Required(ErrorMessage = "Introduce el e-mail del cliente")]
        [Display(Name = "E-mail del Cliente")]
        public String Mail { get; set; }

        [Required(ErrorMessage = "Introduce credito del cliente")]
        [Display(Name = "Credito del Cliente")]
        public int Credit { get; set; }

    }
}
