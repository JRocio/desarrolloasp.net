﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyClient_Core.Models;

namespace MyClient_Core.Controllers
{
    public class MyClientController : Controller
    {
        private readonly ApplicationDbContext d_b;
        public MyClientController (ApplicationDbContext db)
        {
            d_b = db;
        }

        // GET: HomeController1
        public ActionResult Index()
        {
            var displaydata = d_b.Clients.ToList();
            return View(displaydata);
        }

        [HttpGet]
        public async Task<IActionResult> Index(string ClieSearch)
        {
            ViewData["GetClient"] = ClieSearch;
            var clquery = from x in d_b.Clients select x;
            if(!String.IsNullOrEmpty(ClieSearch))
            {
                clquery = clquery.Where(x => x.Cliename.Contains(ClieSearch) || x.Mail.Contains(ClieSearch));
            }
            return View(await clquery.AsNoTracking().ToListAsync());
        }

        // GET: HomeController1/Details/5
        public async Task <IActionResult> Detail(int? id)
        {
            if (id==null)
            {
                return RedirectToAction("Index");
            }
            var getClieDetail = await d_b.Clients.FindAsync(id);
            return View(getClieDetail);
        }

        // GET: HomeController1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HomeController1/Create
        [HttpPost]
        public async Task<IActionResult> Create (Clients newC)
        {
            if(ModelState.IsValid)
            {
                d_b.Add(newC);
                await d_b.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(newC);
        }

        // GET: HomeController1/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id==null)
            {
                return RedirectToAction("Index");
            }
            var getClieDetail = await d_b.Clients.FindAsync(id);
            return View(getClieDetail);
        }

        // POST: HomeController1/Edit/5
        [HttpPost]
        public async Task<IActionResult> Edit(Clients oldC)
        {
            if(ModelState.IsValid)
            {
                d_b.Update(oldC);
                await d_b.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldC);
        }

        // GET: HomeController1/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getClieDetail = await d_b.Clients.FindAsync(id);
            return View(getClieDetail);
        }

        // POST: HomeController1/Delete/5
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
           
                var getClieDetail = await d_b.Clients.FindAsync(id);
                d_b.Clients.Remove(getClieDetail);
                await d_b.SaveChangesAsync();
                return RedirectToAction("Index");
           
        }
    }
}
