﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_EMPLOYEE.Models
{
    [MetadataType(typeof(Employee.Metadata))]
    public partial class Employee
    {
        //Clase que no se puede instanciar mas que desde ella misma
        sealed class Metadata
        {
            [Key]
            public int Empid;
            [Required(ErrorMessage = "Ingresa nombre del empleado")]
            public string Empname;

            [Required(ErrorMessage = "Ingresa email del empleado")]
            public string Email;

            [Required(ErrorMessage = "Ingresa edad valida")]
            [Range (20,60)]
            public Nullable<int> Age;

            [Required(ErrorMessage = "Ingresa salario")]
            public Nullable<int> Salary;

        }

    }
}